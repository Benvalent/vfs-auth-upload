import React from "react";
import { useParams, useLocation } from "react-router-dom";
import { Container } from "react-bootstrap";
import { useFolder } from "../../hooks/useFolder";
import Navbar from "../navigation/Navbar";
import AddFolderButton from "./AddFolderButton";
import FolderBreadcrumbs from "./FolderBreadcrumbs";
import Folder from "./Folder";
import File from "./File";
import AddFileButton from "./AddFileButton";

export default function Dashboard() {
  const { folderId } = useParams();
  const { state = {} } = useLocation();
  const { folder, childFolders, childFiles } = useFolder(
    folderId,
    state.folder
  );

  return (
    <>
      <Navbar />
      <Container>
        <div className="d-flex align-items-center">
          <FolderBreadcrumbs currentFolder={folder} />
          <AddFolderButton currentFolder={folder} />
          <AddFileButton currentFolder={folder} />
        </div>
        <div className="panel mt-3">
          {childFolders.length > 0 && (
            <div className="d-flex flex-wrap">
              {childFolders.map((childFolder) => (
                <div
                  key={childFolder.id}
                  style={{ maxWidth: "250px" }}
                  className="p-2"
                >
                  <Folder folder={childFolder} />
                </div>
              ))}
            </div>
          )}

          {childFolders.length > 0 && childFiles.length > 0 && <hr />}

          {childFiles.length > 0 && (
            <div className="d-flex flex-wrap">
              {childFiles.map((childFile) => (
                <File key={childFile.id} file={childFile} />
              ))}
            </div>
          )}
        </div>
      </Container>
    </>
  );
}
