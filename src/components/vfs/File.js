import { faFile } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export default function File({ file }) {
  return (
    <a
      href={file.url}
      targe="_blank"
      className="file-entry w-100 text-left d-inline-flex justify-content-between"
    >
      <div>
        <FontAwesomeIcon icon={faFile} className="mr-3" />
        {file.name}
      </div>
      <span>{new Date(file.createdAt.seconds * 1000).toDateString()}</span>
    </a>
  );
}
