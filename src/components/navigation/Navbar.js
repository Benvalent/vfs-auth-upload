import React from "react";
import { Navbar as BNavbar, Nav, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <BNavbar bg="dark" variant="dark" expand="sm" className="mb-3">
      <Container>
        <BNavbar.Brand as={Link} to="/">
          VFS Upload
        </BNavbar.Brand>
        <Form.Control
          type="text"
          placeholder="Search"
          style={{ maxWidth: "500px" }}
          className="search-input"
        />
        <Nav>
          <Nav.Link as={Link} to="/profile">
            Profile
          </Nav.Link>
        </Nav>
      </Container>
    </BNavbar>
  );
}
