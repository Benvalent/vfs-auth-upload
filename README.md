# VFS Auth Upload

Small Google Drive simulation application with authentication, authorization, file upload and file listing capabilities.

- **React**: declarative, component-based frontend framework
- **Firebase**
  - **Auth**: easy-to-use authentication library that can be used for authorization as well (AuthContext)
  - **Firestore**: used for creating tables for users and files
  - **Storage**: used for storing the uploaded files


For the styling:
- **react-bootstrap**: Bootstrap based React components (`<Button />`, `<Navbar />`)
- **font-awesome**: vector icons

## Setup
1. Navigate to root folder (`./vfs-auth-upload`) and install dependencies
```js
npm i 
```
2. Create a `.env.local` file in the root directory and add the following variables:
```dart
REACT_APP_FIREBASE_API_KEY=
REACT_APP_FIREBASE_AUTH_DOMAIN=
REACT_APP_FIREBASE_PROJECT_ID=
REACT_APP_FIREBASE_STORAGE_BUCKET=
REACT_APP_FIREBASE_MESSAGING_SENDER_ID=
REACT_APP_FIREBASE_APP_ID=
```
3. Create [new Firebase project](https://console.firebase.google.com/) (it's free)
4. Once created, navigate to `Authentication` in Firebase, select the `Sign-in method` tab and enable Email/Password
5. Go back to the Project Overview page, select the newly created project, click on the `Web app` button and register your app
6. Copy the configuration keys from the `firebaseConfig` object to the previously created `.env.local` file
7. Run the following comman and you should be good to go!
```
npm run start
```

![image](/uploads/25c93b2d68b452aaad73ef8238a74573/image.png)
